<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs" version="2.0">
    
    <xsl:output indent="yes"/>
    
    <xsl:variable name="sglnUrn" select="'urn:epc:id:sgln:'"/>
    <xsl:variable name="bizStepUrn" select="'urn:epcglobal:cbv:bizstep:'"/>
    <xsl:variable name="dispositionUrn" select="'urn:epcglobal:cbv:disp:'"/>
    <xsl:variable name="despatchAdviseUrn" select="'urn:epcglobal:cbv:bt:'"/>
    <xsl:variable name="created" select="current-dateTime()"/>
    
    <xsl:template match="/">
        <xsl:variable name="event" select="/trackAndTrace/@event"/>
        
        <epcis:EPCISDocument schemaVersion="1.2" creationDate="{$created}"
            xmlns:epcis="urn:epcglobal:epcis:xsd:1">
            
        <xsl:call-template name="createHeader" />
       
        <xsl:call-template name="createBody">
            <xsl:with-param name="event" select="$event" />
        </xsl:call-template>  
            
        </epcis:EPCISDocument>
        
    </xsl:template>
    
    <xsl:template name="createHeader">
        <EPCISHeader>
            <StandardBusinessDocumentHeader
                xmlns="http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader">
                <HeaderVersion>1.0</HeaderVersion>
                <Sender>
                    <Identifier Authority="SGLN">
                        <xsl:value-of select="concat($sglnUrn, /trackAndTrace/eanCode)"/>
                    </Identifier>
                </Sender>
                <Receiver>
                    <Identifier Authority="SGLN">
                        <xsl:value-of select="concat($sglnUrn, /trackAndTrace/eanCode)"/>
                    </Identifier>
                </Receiver>
                <DocumentIdentification>
                    <Standard>EPCglobal</Standard>
                    <TypeVersion>1.0</TypeVersion>
                    <InstanceIdentifier>
                        <xsl:value-of select="/trackAndTrace/parent"/>
                    </InstanceIdentifier>
                    <Type>Events</Type>
                    <CreationDateAndTime>
                        <xsl:value-of select="$created"/>
                    </CreationDateAndTime>
                </DocumentIdentification>
            </StandardBusinessDocumentHeader>
        </EPCISHeader>
    </xsl:template>
    
    <xsl:template name="createBody">
        <xsl:param name="event" as="xs:string" />
        
        <EPCISBody>
            <EventList>
                
                <xsl:call-template name="createEvent">
                    <xsl:with-param name="event" select="$event" />
                </xsl:call-template>
                
            </EventList>
        </EPCISBody>
    </xsl:template>
    
    <xsl:template name="createEvent">
        <xsl:param name="event" as="xs:string" />
            
            <xsl:choose>
                <xsl:when test="$event eq 'aggregation'">
                    <xsl:call-template name="createAggregationEvent" />
                </xsl:when>
                <xsl:when test="$event eq 'object'">
                    <xsl:call-template name="createObjectEvent" />
                </xsl:when>
                <xsl:when test="$event eq 'transaction'">
                    <xsl:call-template name="createTransactionEvent" />
                </xsl:when>
            </xsl:choose>
        
    </xsl:template>
    
    <xsl:template name="createAggregationEvent">
        
        <AggregationEvent>
            <eventTime>
                <xsl:value-of select="$created"/>
            </eventTime>
            <eventTimeZoneOffset>+01:00</eventTimeZoneOffset>
            <parentID><xsl:value-of select="/trackAndTrace/parent"/></parentID>
            <childEPCs>
                <xsl:for-each select="/trackAndTrace/productCodes/productCode">
                    <epc>
                        <xsl:value-of select="."/>
                    </epc>
                </xsl:for-each>
            </childEPCs>
            <action>ADD</action>
            <bizStep>
                <xsl:value-of select="concat($bizStepUrn, 'packing')"/>
            </bizStep>
            <disposition>
                <xsl:value-of select="concat($dispositionUrn, 'in_progress')"/>
            </disposition>
            <bizLocation>
                <id>
                    <xsl:value-of
                        select="concat($sglnUrn, /trackAndTrace/businessLocation)"/>
                </id>
            </bizLocation>
            
            <xsl:if test="exists(/trackAndTrace/market) and string-length(/trackAndTrace/market/text()) gt 0">
                <atos:market xmlns:atos="urn:atos:extension:xsd">
                    <xsl:value-of select="/trackAndTrace/market"/>
                </atos:market>    
            </xsl:if>
            
            <atos:bizCountry xmlns:atos="urn:atos:extension:xsd">
                <xsl:value-of select="/trackAndTrace/businessCountry"/>
            </atos:bizCountry>
        </AggregationEvent>
    </xsl:template>
    
    <xsl:template name="createObjectEvent">
        <ObjectEvent>
            <eventTime><xsl:value-of select="$created"/></eventTime>
            <eventTimeZoneOffset>+01:00</eventTimeZoneOffset>
            <epcList>
                <xsl:for-each select="/trackAndTrace/productCodes/productCode">
                    <epc><xsl:value-of select="."/></epc>
                </xsl:for-each>
            </epcList>
            <action>OBSERVE</action>
            <bizStep>
                <xsl:value-of select="concat($bizStepUrn, /trackAndTrace/businessStep)"/>
            </bizStep>
            <disposition>
                <xsl:value-of select="concat($dispositionUrn, /trackAndTrace/disposition)"/>
            </disposition>
            
            <xsl:if test="/trackAndTrace/businessStep eq 'receiving'">
                <bizLocation>
                    <id>
                        <xsl:value-of select="/trackAndTrace/destinationLocation"/>
                    </id>
                </bizLocation>    
            </xsl:if>
            
            <xsl:if test="/trackAndTrace/businessStep eq 'shipping'">
                <readPoint>
                    <id>
                        <xsl:value-of select="concat($sglnUrn, /trackAndTrace/readPoint)"/>
                    </id>
                </readPoint>
                
                <xsl:if test="exists(/trackAndTrace/despatchAdvise) and string-length(/trackAndTrace/despatchAdvise/text()) gt 0">
                    <bizTransactionList>
                        <bizTransaction type="urn:epcglobal:cbv:btt:desadv">
                            <xsl:value-of select="concat($despatchAdviseUrn ,/trackAndTrace/despatchAdvise)"/>
                        </bizTransaction>
                    </bizTransactionList>
                </xsl:if>
                
                <extension>
                    <destinationList>
                        <destination type="urn:epcglobal:cbv:sdt:location">
                            <xsl:value-of select="/trackAndTrace/destinationLocation"/>
                        </destination>
                    </destinationList>
                </extension>
            </xsl:if>
            
            <atos:market xmlns:atos="urn:atos:extension:xsd">
                <xsl:value-of select="/trackAndTrace/market"/>
            </atos:market>
            <atos:bizCountry xmlns:atos="urn:atos:extension:xsd">
                <xsl:value-of select="trackAndTrace/businessCountry"/>
            </atos:bizCountry>
            <tpd:tpdExtension xmlns:tpd="urn:atos:tpdextension:xsd">
                
                <xsl:if test="/trackAndTrace/businessStep eq 'shipping'">
                    <xsl:choose>
                        <xsl:when test="exists(/trackAndTrace/transportMode) and string-length(/trackAndTrace/transportMode/text()) gt 0">
                            <Transport_mode><xsl:value-of select="/trackAndTrace/transportMode"/></Transport_mode>    
                        </xsl:when>
                        <xsl:otherwise><Transport_mode>0</Transport_mode></xsl:otherwise>
                    </xsl:choose>
                    
                    <xsl:choose>
                        <xsl:when test="exists(/trackAndTrace/transportVehicle) and string-length(/trackAndTrace/transportVehicle/text()) gt 0">
                            <Transport_vehicle><xsl:value-of select="/trackAndTrace/transportVehicle"/></Transport_vehicle>
                        </xsl:when>
                        <xsl:otherwise><Transport_vehicle>n/a</Transport_vehicle></xsl:otherwise>
                    </xsl:choose>
                   
                    <xsl:if test="exists(/trackAndTrace/emcsArc) and string-length(/trackAndTrace/emcsArc/text()) gt 0">
                        <EMCS_ARC>
                            <xsl:value-of select="/trackAndTrace/emcsArc"/>
                        </EMCS_ARC>
                    </xsl:if>
                        
                    <xsl:if test="exists(/trackAndTrace/saadNumber) and string-length(/trackAndTrace/saadNumber/text()) gt 0">
                        <SAAD_number>
                            <xsl:value-of select="/trackAndTrace/saadNumber"/>
                        </SAAD_number>
                    </xsl:if>
                    
                    <xsl:if test="exists(/trackAndTrace/expDeclarationNumber) and string-length(/trackAndTrace/expDeclarationNumber/text()) gt 0">
                        <Exp_DeclarationNumber>
                            <xsl:value-of select="/trackAndTrace/expDeclarationNumber"/>
                        </Exp_DeclarationNumber>
                    </xsl:if>
                    
                    <xsl:if test="exists(/trackAndTrace/destinationAddress) and string-length(/trackAndTrace/destinationAddress/text()) gt 0">
                        <Destination_Address>
                            <xsl:value-of select="/trackAndTrace/destinationAddress"/>
                        </Destination_Address>
                    </xsl:if>
                    
                    <xsl:if test="exists(/trackAndTrace/dispatchComment) and string-length(/trackAndTrace/dispatchComment/text()) gt 0">
                        <Dispatch_comment>
                            <xsl:value-of select="/trackAndTrace/dispatchComment"/>
                        </Dispatch_comment>    
                    </xsl:if>
                        
                </xsl:if>
                
                <xsl:if test="/trackAndTrace/businessStep eq 'receiving'">
                    <xsl:if test="exists(/trackAndTrace/arrivalComment) and string-length(/trackAndTrace/arrivalComment/text()) > 0">
                        <Arrival_comment>
                            <xsl:value-of select="/trackAndTrace/arrivalComment"/>
                        </Arrival_comment>    
                    </xsl:if>
                </xsl:if>
                
            </tpd:tpdExtension>
        </ObjectEvent>
    </xsl:template>
    
    <xsl:template name="createTransactionEvent">
        <TransactionEvent>
            <eventTime><xsl:value-of select="$created"/></eventTime>
            <eventTimeZoneOffset>+01:00</eventTimeZoneOffset>
            <bizTransactionList>
                <bizTransaction type="urn:epcglobal:cbv:btt:po">
                    <xsl:value-of select="/trackAndTrace/poOrderNumber"/>
                </bizTransaction>
            </bizTransactionList>
            <epcList>
                <xsl:for-each select="/trackAndTrace/productCodes/productCode">
                    <epc>
                        <xsl:value-of select="."/>
                    </epc>
                </xsl:for-each>
            </epcList>
            <action>ADD</action>
            <bizStep>urn:atos:cbv:bizstep:ordering_directly</bizStep><!-- correct urn ??? -->
            <atos:market xmlns:atos="urn:atos:extension:xsd">
                <xsl:value-of select="/trackAndTrace/market"/>
            </atos:market>
            <atos:bizCountry xmlns:atos="urn:atos:extension:xsd">
                <xsl:value-of select="/trackAndTrace/businessCountry"/>
            </atos:bizCountry>
            <tpd:tpdextension xmlns:tpd="urn:atos:tpdextension:xsd">
                <Order_Date>
                    <xsl:value-of select="/trackAndTrace/orderDate"/>
                </Order_Date>
            </tpd:tpdextension>
        </TransactionEvent>
        
    </xsl:template>
</xsl:stylesheet>
