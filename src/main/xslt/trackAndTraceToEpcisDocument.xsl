<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs" version="2.0">
    
    <xsl:output indent="yes"/>
    
    <xsl:variable name="sglnUrn" select="'urn:epc:id:sgln:'"/>
    <xsl:variable name="locUrn" select="'urn:atos:loc:'"/>
    <xsl:variable name="locTpdUrn" select="'urn:atos:loc:tpd:'"/>
    <xsl:variable name="locTpdEoidUrn" select="'urn:atos:loc:tpd:eoid:'"/>
    <xsl:variable name="bizStepUrn" select="'urn:epcglobal:cbv:bizstep:'"/>
    <xsl:variable name="bizStepUrnTransaction" select="'urn:atos:cbv:bizstep:'"/> <!-- correct? -->
    <xsl:variable name="dispositionUrn" select="'urn:epcglobal:cbv:disp:'"/>
    <xsl:variable name="businessTransactionUrn" select="'urn:epcglobal:cbv:bt:'"/>
    <xsl:variable name="purchaseOrderType" select="'urn:epcglobal:cbv:btt:po'"/>
    <xsl:variable name="dispatchAdviceType" select="'urn:epcglobal:cbv:btt:desadv'"/>
    <xsl:variable name="invoiceType" select="'urn:epcglobal:cbv:btt:inv'"/>
    <xsl:variable name="extensionType" select="'urn:epcglobal:cbv:sdt:owning_party'"/>
    <xsl:variable name="created" select="/trackAndTraceEvents/trackAndTrace/currentDateTime"/>
    
    <xsl:template match="/">
        
        
        <epcis:EPCISDocument schemaVersion="1.2" creationDate="{$created}"
            xmlns:epcis="urn:epcglobal:epcis:xsd:1">
            
        <xsl:call-template name="createHeader" />
       
        <xsl:call-template name="createBody" />
            
        </epcis:EPCISDocument>
        
    </xsl:template>
    
    <xsl:template name="createHeader">
        <EPCISHeader>
            <StandardBusinessDocumentHeader
                xmlns="http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader">
                <HeaderVersion>1.0</HeaderVersion>
                <Sender>
                    <Identifier Authority="SGLN">
                        <xsl:value-of select="concat($sglnUrn, /trackAndTraceEvents/@eanCode)"/>
                    </Identifier>
                </Sender>
                <Receiver>
                    <Identifier Authority="SGLN">
                        <xsl:value-of select="concat($sglnUrn, /trackAndTraceEvents/@eanCode)"/>
                    </Identifier>
                </Receiver>
                <DocumentIdentification>
                    <Standard>EPCglobal</Standard>
                    <TypeVersion>1.0</TypeVersion>
                    <InstanceIdentifier>
                        <xsl:value-of select="/trackAndTraceEvents/@messageIdentifier"/>
                    </InstanceIdentifier>
                    <Type>Events</Type>
                    <CreationDateAndTime>
                        <xsl:value-of select="$created"/>
                    </CreationDateAndTime>
                </DocumentIdentification>
            </StandardBusinessDocumentHeader>
        </EPCISHeader>
    </xsl:template>
    
    <xsl:template name="createBody">
        
        <EPCISBody>
            <EventList>
                
                <xsl:call-template name="createEvent" />
                
            </EventList>
        </EPCISBody>
    </xsl:template>
    
    <xsl:template name="createEvent">
            
            <xsl:for-each select="/trackAndTraceEvents/trackAndTrace">
                <xsl:choose>
                    <xsl:when test="./@event = 'aggregation'">
                        <xsl:call-template name="createAggregationEvent" />
                    </xsl:when>
                    <xsl:when test="./@event = 'object'">
                        <xsl:call-template name="createObjectEvent" />
                    </xsl:when>
                    <xsl:when test="./@event = 'transaction'">
                        <xsl:call-template name="createTransactionEvent" />
                    </xsl:when>
                </xsl:choose>    
            </xsl:for-each>
            
    </xsl:template>
    
    <xsl:template name="createAggregationEvent" match="/trackAndTraceEvents/trackAndTrace">
        
        <AggregationEvent>
            <eventTime>
                <xsl:value-of select="$created"/>
            </eventTime>
            <eventTimeZoneOffset>+01:00</eventTimeZoneOffset>
            <parentID><xsl:value-of select="./parent"/></parentID>
            <childEPCs>
                <xsl:for-each select="./productCodes/productCode">
                    <epc>
                        <xsl:value-of select="."/>
                    </epc>
                </xsl:for-each>
            </childEPCs>
            <action>ADD</action>
            <bizStep>
                <xsl:value-of select="concat($bizStepUrn, 'packing')"/>
            </bizStep>
            <disposition>
                <xsl:value-of select="concat($dispositionUrn, 'in_progress')"/>
            </disposition>
            <bizLocation>
                <id>
                    <xsl:value-of
                        select="concat($sglnUrn, ./businessLocation)"/>
                </id>
            </bizLocation>
            
            <xsl:if test="./market and string-length(./market/text()) > 0">
                <atos:market xmlns:atos="urn:atos:extension:xsd">
                    <xsl:value-of select="./market"/>
                </atos:market>    
            </xsl:if>
            
            <atos:bizCountry xmlns:atos="urn:atos:extension:xsd">
                <xsl:value-of select="./businessCountry"/>
            </atos:bizCountry>
        </AggregationEvent>
    </xsl:template>
    
    <xsl:template name="createObjectEvent" match="/trackAndTraceEvents/trackAndTrace">
        <ObjectEvent>
            <eventTime><xsl:value-of select="$created"/></eventTime>
            <eventTimeZoneOffset>+01:00</eventTimeZoneOffset>
            <epcList>
                <xsl:for-each select="./productCodes/productCode">
                    <epc><xsl:value-of select="."/></epc>
                </xsl:for-each>
            </epcList>
            <action>OBSERVE</action>
            <bizStep>
                <xsl:value-of select="concat($bizStepUrn, ./businessStep)"/>
            </bizStep>
            <disposition>
                <xsl:value-of select="concat($dispositionUrn, ./disposition)"/>
            </disposition>
            
            <xsl:if test="./businessStep = 'receiving'">
                <bizLocation>
                    <id>
                        <xsl:value-of select="concat($sglnUrn, ./destinationLocation)"/>
                    </id>
                </bizLocation>    
            </xsl:if>
            
            <xsl:if test="./businessStep = 'shipping'">
                <readPoint>
                    <id>
                        <xsl:value-of select="concat($sglnUrn, ./readPoint)"/>
                    </id>
                </readPoint>
                
                <xsl:if test="./despatchAdvise and string-length(./despatchAdvise/text())  > 0">
                    <bizTransactionList>
                        <bizTransaction type="urn:epcglobal:cbv:btt:desadv">
                            <xsl:value-of select="concat($businessTransactionUrn , ./despatchAdvise)"/>
                        </bizTransaction>
                    </bizTransactionList>
                </xsl:if>
                
                <extension>
                    <destinationList>
                        <destination type="urn:epcglobal:cbv:sdt:location">
                            <xsl:if test="./destinationLocation and string-length(./destinationLocation/text())  > 0">
                                <xsl:choose>
                                    <xsl:when test="./@businessEvent = 'customerOrder'">
                                        <xsl:value-of select="concat($locUrn, ./destinationLocation)"/>        
                                    </xsl:when>
                                    <xsl:when test="./@businessEvent = 'distributionOrder'">
                                        <xsl:value-of select="concat($sglnUrn, ./destinationLocation)"/>        
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="concat($sglnUrn, ./destinationLocation)"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                        </destination>
                    </destinationList>
                </extension>
            </xsl:if>
            
            <atos:market xmlns:atos="urn:atos:extension:xsd">
                <xsl:value-of select="./market"/>
            </atos:market>
            <atos:bizCountry xmlns:atos="urn:atos:extension:xsd">
                <xsl:value-of select="./businessCountry"/>
            </atos:bizCountry>
            <tpd:tpdExtension xmlns:tpd="urn:atos:tpdextension:xsd">
                
                <xsl:if test="./businessStep = 'shipping'">
                    <xsl:choose>
                        <xsl:when test="./transportMode and string-length(./transportMode/text())  > 0">
                            <Transport_mode><xsl:value-of select="./transportMode"/></Transport_mode>    
                        </xsl:when>
                        <xsl:otherwise><Transport_mode>0</Transport_mode></xsl:otherwise>
                    </xsl:choose>
                    
                    <xsl:choose>
                        <xsl:when test="./transportVehicle and string-length(./transportVehicle/text())  > 0">
                            <Transport_vehicle><xsl:value-of select="./transportVehicle"/></Transport_vehicle>
                        </xsl:when>
                        <xsl:otherwise><Transport_vehicle>n/a</Transport_vehicle></xsl:otherwise>
                    </xsl:choose>
                   
                    <xsl:if test="./emcsArc and string-length(./emcsArc/text())  > 0">
                        <EMCS_ARC>
                            <xsl:value-of select="./emcsArc"/>
                        </EMCS_ARC>
                    </xsl:if>
                        
                    <xsl:if test="./saadNumber and string-length(./saadNumber/text())  > 0">
                        <SAAD_number>
                            <xsl:value-of select="/trackAndTrace/saadNumber"/>
                        </SAAD_number>
                    </xsl:if>
                    
                    <xsl:if test="./expDeclarationNumber and string-length(./expDeclarationNumber/text())  > 0">
                        <Exp_DeclarationNumber>
                            <xsl:value-of select="./expDeclarationNumber"/>
                        </Exp_DeclarationNumber>
                    </xsl:if>
                    
                    <xsl:if test="./destinationAddress and string-length(./destinationAddress/text())  > 0">
                        <Destination_Address>
                            <xsl:value-of select="./destinationAddress"/>
                        </Destination_Address>
                    </xsl:if>
                    
                    <xsl:if test="./dispatchComment and string-length(./dispatchComment/text())  > 0">
                        <Dispatch_comment>
                            <xsl:value-of select="./dispatchComment"/>
                        </Dispatch_comment>    
                    </xsl:if>
                        
                </xsl:if>
                
                <xsl:if test="./businessStep = 'receiving'">
                    <xsl:if test="./arrivalComment and string-length(./arrivalComment/text()) > 0">
                        <Arrival_comment>
                            <xsl:value-of select="./arrivalComment"/>
                        </Arrival_comment>    
                    </xsl:if>
                </xsl:if>
                
            </tpd:tpdExtension>
        </ObjectEvent>
    </xsl:template>
    
    <xsl:template name="createTransactionEvent" match="/trackAndTraceEvents/trackAndTrace">
        <TransactionEvent>
            <eventTime><xsl:value-of select="$created"/></eventTime>
            <eventTimeZoneOffset>+01:00</eventTimeZoneOffset>
            <bizTransactionList>
                <xsl:for-each select="./businessTransactions/businessTransaction">
                    <xsl:choose>
                        <xsl:when test="./@businessType = 'purchase order'">
                            <bizTransaction type="{$purchaseOrderType}">
                                <xsl:value-of select="concat($businessTransactionUrn,.)"/>
                            </bizTransaction>        
                        </xsl:when>
                        <xsl:when test="./@businessType = 'dispatch advice'">
                            <bizTransaction type="{$dispatchAdviceType}">
                                <xsl:value-of select="concat($businessTransactionUrn,.)"/>
                            </bizTransaction>
                        </xsl:when>
                        <xsl:when test="./@businessType = 'invoice'">
                            <bizTransaction type="{$invoiceType}">
                                <xsl:value-of select="concat($businessTransactionUrn,.)"/>
                            </bizTransaction>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
            </bizTransactionList>
            <epcList>
                <xsl:for-each select="./productCodes/productCode">
                    <epc>
                        <xsl:value-of select="."/>
                    </epc>
                </xsl:for-each>
            </epcList>
            <action>ADD</action>
            <bizStep>
                <xsl:value-of select="concat($bizStepUrnTransaction, ./businessStep)"/>
            </bizStep>
            
            <extension>
                <sourceList>
                    <source type="{$extensionType}">
                        <xsl:if test="./source and string-length(./source/text()) > 0">
                            <xsl:choose>
                                <xsl:when test="./businessStep = 'paying'">
                                    <xsl:value-of select="concat($locTpdUrn, ./source)"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="concat($sglnUrn, ./source)"/>        
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>      
                    </source>
                </sourceList>
                    <destinationList>
                        <destination type="{$extensionType}">
                            <xsl:if test="./destination and string-length(./destination/text()) > 0">
                                <xsl:choose>
                                    <xsl:when test="./businessStep = 'invoicing'">
                                        <xsl:value-of select="concat($locTpdEoidUrn, ./destination)"/>        
                                    </xsl:when>
                                    <xsl:when test="./businessStep = 'paying'">
                                        <xsl:value-of select="concat($sglnUrn, ./destination)"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="concat($sglnUrn, ./destination)"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:if>
                        </destination>
                    </destinationList>    
            </extension>
            <atos:market xmlns:atos="urn:atos:extension:xsd">
                <xsl:value-of select="./market"/>
            </atos:market>
            <atos:bizCountry xmlns:atos="urn:atos:extension:xsd">
                <xsl:value-of select="./businessCountry"/>
            </atos:bizCountry>
            <tpd:tpdextension xmlns:tpd="urn:atos:tpdextension:xsd">
                <xsl:if test="./businessStep = 'invoicing'">
                    <Buyer_Name>
                        <xsl:value-of select="./buyerName"/>
                    </Buyer_Name>
                    <Buyer_Address>
                        <xsl:value-of select="./buyerAddress"/>
                    </Buyer_Address>
                    <Buyer_CountryReg>
                        <xsl:value-of select="./buyerCountryReg"/>    
                    </Buyer_CountryReg>
                    <Buyer_TAX_N>
                        <xsl:value-of select="./buyerTaxNumber"/>
                    </Buyer_TAX_N>
                    <Invoice_Type1>
                        <xsl:value-of select="./invoiceType"/>
                    </Invoice_Type1>
                    <Invoice_Date>
                        <xsl:value-of select="./invoiceDate"/>
                    </Invoice_Date>
                    <First_Seller_EU>
                        <xsl:value-of select="./firstSellerEU"/>
                    </First_Seller_EU>
                    <Product_Items_1>
                        <xsl:for-each select="./productTpidItems/productTpidItem">
                            <id>
                                <xsl:value-of select="."/>
                            </id>    
                        </xsl:for-each>
                    </Product_Items_1>
                    <Product_Items_2>
                        <xsl:for-each select="./productPnItems/productPnItem">
                            <id>
                                <xsl:value-of select="."/>
                            </id>    
                        </xsl:for-each>
                    </Product_Items_2>
                    <Product_Price>
                        <xsl:for-each select="./productPrices/productPrice">
                            <id>
                                <xsl:value-of select="."/>
                            </id>    
                        </xsl:for-each>
                    </Product_Price>
                    <Invoice_Net>
                        <xsl:value-of select="./invoiceNet"/>
                    </Invoice_Net>
                    <Invoice_Currency>
                        <xsl:value-of select="./invoiceCurrency"/>
                    </Invoice_Currency>
                </xsl:if>
                
                <xsl:if test="./businessStep = 'paying'">
                    <Payment_Date>
                        <xsl:value-of select="./paymentDate"/>
                    </Payment_Date>
                    <Payment_Type>
                        <xsl:value-of select="./paymentType"/>
                    </Payment_Type>
                    <Payment_Amount>
                        <xsl:value-of select="./paymentAmount"/>
                    </Payment_Amount>
                    <Payment_Currency>
                        <xsl:value-of select="./paymentCurrency"/>
                    </Payment_Currency>
                    <Payer_Name>
                        <xsl:value-of select="./payerName"/>
                    </Payer_Name>
                    <Payer_Address>
                        <xsl:value-of select="./payerAddress"/>
                    </Payer_Address>
                    <Payer_CountryReg>
                        <xsl:value-of select="./payerCountryReg"/>
                    </Payer_CountryReg>
                    <Payer_TAX_N>
                        <xsl:value-of select="./payerTaxNumber"/>
                    </Payer_TAX_N>
                    
                </xsl:if>
                
                <xsl:if test="./businessStep = 'ordering_directly'">
                    <Order_Date>
                        <xsl:value-of select="./orderDate"/>
                    </Order_Date>
                </xsl:if>
            </tpd:tpdextension>
        </TransactionEvent>
        
    </xsl:template>
</xsl:stylesheet>
